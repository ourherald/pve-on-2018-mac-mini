# Proxmox (via Debian) on MacMini 2018

- Install Debian 11 on MacMini

`sudo vi /etc/apt/sources.list`

- Add: deb http://deb.debian.org/debian bullseye main contrib

`sudo apt install --yes openssh-server`
`sudo systemctl restart ssh`

- SSH into machine

````
gsettings set org.gnome.desktop.media-handling automount false
sudo -i
apt install --yes debootstrap gdisk zfsutils-linux
````

- Setup disk
````
ll /dev/disk/by-id
export DISK=/dev/disk/by-id/scsi-SATA_disk1
export HOSTNAME=pve01
swapoff --all

wipefs -a $DISK
sgdisk --zap-all $DISK
sgdisk     -n2:1M:+512M   -t2:EF00 $DISK
sgdisk     -n3:0:+1G      -t3:BF01 $DISK
sgdisk     -n4:0:0        -t4:BF00 $DISK

zpool create \
    -o ashift=12 \
    -o autotrim=on -d \
    -o cachefile=/etc/zfs/zpool.cache \
    -o feature@async_destroy=enabled \
    -o feature@bookmarks=enabled \
    -o feature@embedded_data=enabled \
    -o feature@empty_bpobj=enabled \
    -o feature@enabled_txg=enabled \
    -o feature@extensible_dataset=enabled \
    -o feature@filesystem_limits=enabled \
    -o feature@hole_birth=enabled \
    -o feature@large_blocks=enabled \
    -o feature@livelist=enabled \
    -o feature@lz4_compress=enabled \
    -o feature@spacemap_histogram=enabled \
    -o feature@zpool_checkpoint=enabled \
    -O devices=off \
    -O acltype=posixacl -O xattr=sa \
    -O compression=lz4 \
    -O normalization=formD \
    -O relatime=on \
    -O canmount=off -O mountpoint=/boot -R /mnt \
    bpool ${DISK}-part3
    
zpool create \
    -o ashift=12 \
    -o autotrim=on \
    -O acltype=posixacl -O xattr=sa -O dnodesize=auto \
    -O compression=lz4 \
    -O normalization=formD \
    -O relatime=on \
    -O canmount=off -O mountpoint=/ -R /mnt \
    rpool ${DISK}-part4
    
zfs create -o canmount=off -o mountpoint=none rpool/ROOT
zfs create -o canmount=off -o mountpoint=none bpool/BOOT

zfs create -o canmount=noauto -o mountpoint=/ rpool/ROOT/debian
zfs mount rpool/ROOT/debian

zfs create -o mountpoint=/boot bpool/BOOT/debian


zfs create                     rpool/home
zfs create -o mountpoint=/root rpool/home/root
chmod 700 /mnt/root
zfs create -o canmount=off     rpool/var
zfs create -o canmount=off     rpool/var/lib
zfs create                     rpool/var/log
zfs create                     rpool/var/spool
zfs create -o com.sun:auto-snapshot=false rpool/var/cache
zfs create -o com.sun:auto-snapshot=false rpool/var/lib/nfs
zfs create -o com.sun:auto-snapshot=false rpool/var/tmp
chmod 1777 /mnt/var/tmp
zfs create rpool/srv
````

- Do NOT make rpool/etc or rpool/etc/pve! PVE needs access before the dataset is loaded.

````
zfs create -o com.sun:auto-snapshot=false rpool/var/lib/docker
zfs create rpool/var/mail
`````
## tmpfs
````
mkdir /mnt/run
mount -t tmpfs tmpfs /mnt/run
mkdir /mnt/run/lock
`````
## Install Debian
````
deboostrap bullseye /mnt

mkdir /mnt/etc/zfs
cp /etc/zfs/zpool.cache /mnt/etc/zfs/
````

## Setup Network, etc.
````
fqdn=example.com
localdn=lan

hostname $HOSTNAME
hostname > /mnt/etc/hostname
vi /mnt/etc/hosts
````
- add: `127.0.1.1       $HOSTNAME.${fqdn} $HOSTNAME.${localdn} $HOSTNAME`

`vi /mnt/etc/network/interfaces`

- add:
````
auto $INTERFACE
iface $INTERFACE manual

auto vmbr0
iface vmbr0 inet dhcp
    bridge-ports $INTERFACE
    bridge-stp off
    bridge-fd 0
    bridge-vlan-aware yes
    bridge-vids 2-4094
        
vi /mnt/etc/apt/sources.list
````
- add:
````
deb http://deb.debian.org/debian bullseye main contrib
deb-src http://deb.debian.org/debian bullseye main contrib

deb http://deb.debian.org/debian-security bullseye-security main contrib
deb-src http://deb.debian.org/debian-security bullseye-security main contrib

deb http://deb.debian.org/debian bullseye-updates main contrib
deb-src http://deb.debian.org/debian bullseye-updates main contrib
````
## Bind mount for chroot
````
mount --make-private --rbind /dev  /mnt/dev
mount --make-private --rbind /proc /mnt/proc
mount --make-private --rbind /sys  /mnt/sys
chroot /mnt /usr/bin/env DISK=$DISK bash --login

ln -s /proc/self/mounts /etc/mtab
apt update

apt install --yes console-setup locales

dpkg-reconfigure locales tzdata keyboard-configuration console-setup

apt install --yes dpkg-dev linux-headers-generic linux-image-generic

apt install --yes zfs-initramfs

echo REMAKE_INITRD=yes > /etc/dkms/zfs.conf

apt install dosfstools

mkdosfs -F 32 -s 1 -n EFI ${DISK}-part2
mkdir /boot/efi
echo /dev/disk/by-uuid/$(blkid -s UUID -o value ${DISK}-part2) \
   /boot/efi vfat defaults 0 0 >> /etc/fstab
mount /boot/efi
apt install --yes grub-efi-amd64 shim-signed

apt purge --yes os-prober

passwd
````

`vi /etc/systemd/system/zfs-import-bpool.service`

- add:
````
[Unit]
DefaultDependencies=no
Before=zfs-import-scan.service
Before=zfs-import-cache.service

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/sbin/zpool import -N -o cachefile=none bpool
# Work-around to preserve zpool cache:
ExecStartPre=-/bin/mv /etc/zfs/zpool.cache /etc/zfs/preboot_zpool.cache
ExecStartPost=-/bin/mv /etc/zfs/preboot_zpool.cache /etc/zfs/zpool.cache

[Install]
WantedBy=zfs-import.target
````
## Create /tmp stuff
````
cp /usr/share/systemd/tmp.mount /etc/systemd/system/
systemctl enable tmp.mount
````
## Install SSH
`apt install --yes openssh-server`

`vi /etc/ssh/sshd_config`
- Set: 
`PermitRootLogin yes`

`apt install --yes popularity-contest`

## Setup grub
````
grub-probe /boot
update-initramfs -c -k all
````

`vi /etc/default/grub`
- Set: 
`GRUB_CMDLINE_LINUX="root=ZFS=rpool/ROOT/debian"`

- Remove quiet from: `GRUB_CMDLINE_LINUX_DEFAULT`
- Uncomment: `GRUB_TERMINAL=console`

````
update-grub
grub-install --target=x86_64-efi --efi-directory=/boot/efi \
    --bootloader-id=debian --recheck --no-floppy
    
mkdir /etc/zfs/zfs-list.cache
touch /etc/zfs/zfs-list.cache/bpool
touch /etc/zfs/zfs-list.cache/rpool
zed -F &
````
## Verify zfs cache
````
cat /etc/zfs/zfs-list.cache/bpool
cat /etc/zfs/zfs-list.cache/rpool

fg
````
- Press Ctrl-C.

`sed -Ei "s|/mnt/?|/|" /etc/zfs/zfs-list.cache/*`



## First boot
````
zfs snapshot bpool/BOOT/debian@install
zfs snapshot rpool/ROOT/debian@install

exit

mount | grep -v zfs | tac | awk '/\/mnt/ {print $3}' | \
    xargs -i{} umount -lf {}
zpool export -a

reboot
````

## Install refind
- Boot into live installer again and find ID of disk

`apt install refind /dev/nvme0n1s2`

`reboot`

## After reboot, ssh in as root
`apt dist-upgrade --yes`
`tasksel --new-install`

````
for file in /etc/logrotate.d/* ; do
    if grep -Eq "(^|[^#y])compress" "$file" ; then
        sed -i -r "s/(^|[^#y])(compress)/\1#\2/" "$file"
    fi
done

reboot
````
## Install Proxmox

- Add to /etc/hosts:
`<ip-address>    $HOSTNAME.ourherald.net $HOSTNAME`


## If not, do this:
`vim /etc/apt/sources.d/pve-community.list`

- Add:
`deb [arch=amd64] http://download.proxmox.com/debian/pve bullseye pve-no-subscription`

`wget https://enterprise.proxmox.com/debian/proxmox-release-bullseye.gpg -O /etc/apt/trusted.gpg.d/proxmox-release-bullseye.gpg `
`apt update`

````
apt install proxmox-ve postfix open-iscsi
apt install ifupdown2
````